<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aksesoris_model extends CI_Model {

	public function getAksesoris(){
		return $this->db->get('barang');
	}

	public function getPembelian(){
		$this->db->order_by('tanggal','DESC');
		return $this->db->get('pembelian');
	}

	public function getPembelianNow($where){
		return $this->db->get_where('pembelian', $where);
	}

	public function getKategori($where){
		return $this->db->get_where('barang', $where);
	}

	public function insertAksesoris($acc){
		$this->db->insert('barang',$acc);
		return $this->db->affected_rows();
	}

	public function insertRegister($acc, $profile){
		$this->db->insert('pengguna',$acc);
		$this->db->insert('data_pengguna',$profile);
		return $this->db->affected_rows();
	}

	public function insertProfile($acc, $email, $user){
		$this->db->where('email',$email);
		$this->db->update('data_pengguna',$acc);
		$this->db->where('email',$email);
		$this->db->update('pengguna',$user);
		return $this->db->affected_rows();
	}

	public function insertPembelian($acc){
		$this->db->insert('pembelian',$acc);
		return $this->db->affected_rows();
	}

	public function updateAksesoris($acc, $id){
		$this->db->where('id_brg',$id);
		return $this->db->update('barang',$acc);
		// return $this->db->affected_rows();
	}

	public function getAksesorisById($id)
	{	
		return $this->db->get_where('barang',['id_brg' => $id])->result();
	}

	public function getProfile($email)
	{	
		return $this->db->get_where('data_pengguna',['email' => $email]);
	}

	public function deleteAksesorisById($id)
	{	
		 $this->db->where("id_brg", $id);
		 return $this->db->delete("barang");
	}

	public function cek_signin($where)
	{	
		return $this->db->get_where('pengguna',$where);
	}
}

?>
