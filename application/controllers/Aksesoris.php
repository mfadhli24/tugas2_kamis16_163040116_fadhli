<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Aksesoris extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see https://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->library('session');
		$this->load->library('cart');
	}

	// --------------- Main View -------------------------------------------------------------------

	public function index()
	{
		$this->load->model('Aksesoris_model');
		$cek = $this->Aksesoris_model->getAksesoris();
		$data['acc'] = $cek->result();
		$data['content'] = 'index';
		$this->load->view('templates/guest', $data);
	}

	public function admin()
	{	
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			$this->load->model('Aksesoris_model');
			$cek = $this->Aksesoris_model->getAksesoris();
			$data['acc'] = $cek->result();
			$data['content'] = 'admin/index';
			$this->load->view('templates/admin', $data);
		} else {
			$this->index();
		}
	}

	public function user()
	{	
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "user") {
			$this->load->model('Aksesoris_model');
			$cek = $this->Aksesoris_model->getAksesoris();
			$data['acc'] = $cek->result();
			$data['content'] = 'user/index';
			$this->load->view('templates/user', $data);
		} else {
			$this->index();
		}
	}

	// --------------- Main View ---------------------------------------------------------------------------

	// --------------- Proses Login & Logout ---------------------------------------------------------------

	public function login()
	{
		$data['content'] = 'login/login';
		$this->load->view('login/login');
	}

	public function logout()
	{
		$this->session->sess_destroy();
		$this->index();
		// $data['content'] = 'index';
		// $this->load->view('templates/user', $data);
	}

	public function signin(){
		if (isset($_POST['submit'])) {
			$this->cart->destroy();
			$email = $this->input->post('email');
			$password = $this->input->post('password');
			$where = array(
				'email' => $email,
				'password' => md5($password)
			);
			$this->load->model('Aksesoris_model');
			$cek = $this->Aksesoris_model->cek_signin($where);
			$row = $cek->row_array();
			if ($cek->num_rows() > 0) {
				// $cek = $this->db->affected_rows();
				$data_session = array(
				'user' => $row['user'],
				'email' => $email,
				'password' => md5($password),
				'hak_akses' => $row['hak_akses']
				);
				$this->session->set_userdata($data_session);
				if ($row['hak_akses'] == "admin") {
						$this->admin();
					} else {
						$this->user();
				}
			} else {
				$this->session->set_flashdata('login', '<small><p class="text-danger">Email atau password Salah.</p></small>');	
				$this->login();
			}
		}
	}

	// --------------- Proses Login & Logout ------------------------------------------------------------

	// --------------- Proses Registrasi ----------------------------------------------------------------

	public function register()
	{
		$this->load->view('register/register');
	}

	public function add_register()
	{
		$this->_rules_register();

		$user = $this->input->post('user');
		$email = $this->input->post('email');
		$password1 = $this->input->post('password1');
		$password2 = $this->input->post('password2');
		$check = $this->input->post('check');

		$this->load->model('Aksesoris_model');
		if($this->form_validation->run() == FALSE){
			$this->register();
		} else {
			if ($password1 == $password2) {
			$cek = $this->db->query("SELECT * FROM pengguna where email ='".$email."'")->num_rows();
				if ($cek <= 0) {
					$acc = array(
					'user' => $user,
					'email' => $email,
					'password' => md5($password1),
					'hak_akses' => 'user'
					);

					$profile = array(
					'user' => $user,
					'email' => $email,
					'alamat' => '',
					'kode_pos' => ''
					);
				$status = $this->Aksesoris_model->insertRegister($acc, $profile);
				$this->login();
				} else {
					$this->register();
					$this->session->set_flashdata('email', '<small><p class="text-danger">Email Sudah Terdaftar</p></small>');	
				}
			} else {
				$this->session->set_flashdata('password', '<small><p class="text-danger">Password Tidak Cocok</p></small>');
				$this->register();
			}
			
		}
	}

	// --------------- Proses Registrasi -----------------------------------------------------------------

	// --------------- Proses CRUD -----------------------------------------------------------------------

	public function insert()
	{
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			$this->load->model('Aksesoris_model');
			$data['acc'] = $this->Aksesoris_model->getAksesoris();
			$data['content'] = 'admin/insert';
			$this->load->view('templates/admin', $data);
		} else {
			$this->index();
		}
	}

	public function add() 
	{

		$this->_rules()	;
		$id_brg = $this->input->post('id_brg');
		$nama_brg = $this->input->post('nama_brg');
		$harga_brg = $this->input->post('harga_brg');
		$jumlah_brg = $this->input->post('jumlah_brg');
		$kategori = $this->input->post('kategori');
		$gambar = $this->_uploadImage();
		$this->load->model('Aksesoris_model');
		if($this->form_validation->run() == FALSE){
			$this->insert();
		} else {
			$acc = array(
				'id_brg' => $id_brg,
				'nama_brg' => $nama_brg,
				'harga_brg' => $harga_brg,
				'stok_brg' => $jumlah_brg,
				'kategori' => $kategori,
				'gambar_brg' => $gambar
			);
			$status = $this->Aksesoris_model->insertAksesoris($acc);
			$this->admin();
		}
	}

	public function kategori($kategori)
	{
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "user") {
			$where = array(
				'kategori' => $kategori
			);
			$this->load->model('Aksesoris_model');
			$cek = $this->Aksesoris_model->getKategori($where);
			$data['acc'] = $cek->result();
			$data['content'] = 'user/index';
			$this->load->view('templates/user', $data);
		} else {
			$where = array(
				'kategori' => $kategori
			);
			$this->load->model('Aksesoris_model');
			$cek = $this->Aksesoris_model->getKategori($where);
			$data['acc'] = $cek->result();
			$data['content'] = 'index';
			$this->load->view('templates/guest', $data);
		}
	}

	public function delete($id)
	{
		$this->load->model('Aksesoris_model');
		$data['acc'] = $this->Aksesoris_model->deleteAksesorisById($id);
		$this->admin();
	}

	public function update() 
	{
		if (isset($_POST['submit'])) {
			$id_brg = $this->input->post('id_brg');
			$nama_brg = $this->input->post('nama_brg');
			$harga_brg = $this->input->post('harga_brg');
			$jumlah_brg = $this->input->post('jumlah_brg');
			$kategori = $this->input->post('kategori');
			
			if (!empty($_FILES["gambar_brg"]["name"])) {
				$gambar_brg = $this->_uploadImage();
			} else {
				$gambar_brg = $this->input->post('gambar');
			}
			$this->load->model('Aksesoris_model');
			$acc = array(
				'nama_brg' => $nama_brg,
				'harga_brg' => $harga_brg,
				'stok_brg' => $jumlah_brg,
				'kategori' => $kategori,
				'gambar_brg' => $gambar_brg
			);
			$id = $id_brg;
			$status = $this->Aksesoris_model->updateAksesoris($acc, $id);
			$this->admin();
		}
	}

	public function edit($id)
	{
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			$this->load->model('Aksesoris_model');
			$data['acc'] = $this->Aksesoris_model->getAksesorisById($id);
			$data['content'] = 'admin/update';
			$this->load->view('templates/admin', $data);
		} else {
			$this->index();
		}
	}

	// --------------- Proses CRUD ----------------------------------------------------------------------

	// --------------- Profile --------------------------------------------------------------------------

	public function profile()
	{
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			$this->load->model('Aksesoris_model');
			$this->load->model('Aksesoris_model');
			$email = $this->session->userdata('email');
			$password = $this->session->userdata('password');
			$data['acc'] = $this->Aksesoris_model->getProfile($email)->row_array();
			$data['content'] = 'profile/profile';
			$this->load->view('templates/admin', $data);
		} else {
			$this->load->model('Aksesoris_model');
			$email = $this->session->userdata('email');
			$password = $this->session->userdata('password');
			$data['acc'] = $this->Aksesoris_model->getProfile($email)->row_array();
			$data['content'] = 'profile/profile';
			$this->load->view('templates/user', $data);
		}
	}

	public function edit_profile(){
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			if (isset($_POST['submit'])) {
			$user = $this->input->post('user');
			$email = $this->input->post('email');
			$alamat = $this->input->post('alamat');
			$kode_pos = $this->input->post('kode_pos');
			
			$this->load->model('Aksesoris_model');
				$acc = array(
					'user' => $user,
					'alamat' => $alamat,
					'kode_pos' => $kode_pos
				);

				$user = array(
					'user' => $user,
				);

				$this->session->set_flashdata('profile', '<small><p class="text-Success">Profile berhasil di update.</p></small>');
				$status = $this->Aksesoris_model->insertProfile($acc, $email, $user);
				$this->logout();
			}
		} else {
			if (isset($_POST['submit'])) {
			$user = $this->input->post('user');
			$email = $this->input->post('email');
			$alamat = $this->input->post('alamat');
			$kode_pos = $this->input->post('kode_pos');
			
			$this->load->model('Aksesoris_model');
				$acc = array(
					'user' => $user,
					'alamat' => $alamat,
					'kode_pos' => $kode_pos
				);

				$user = array(
					'user' => $user,
				);

				$this->session->set_flashdata('profile', '<small><p class="text-Success">Profile berhasil di update.</p></small>');
				$status = $this->Aksesoris_model->insertProfile($acc, $email, $user);
				$this->logout();
			}
		}
	}

	// --------------- Profile -----------------------------------------------------------------------

	// --------------- Cart --------------------------------------------------------------------------

	public function add_to_cart(){ //fungsi Add To Cart
		$this->load->model('Aksesoris_model');
		$cek = $this->Aksesoris_model->cek_signin($where);
		$row = $cek->row_array();
		$acc = array(
				'id' => $this->input->post('produk_id'), 
				'name' => $this->input->post('produk_nama'), 
				'price' => $this->input->post('produk_harga'), 
				'qty' => $this->input->post('quantity'), 
			);
		$this->cart->insert($acc);
		echo $this->show_cart(); //tampilkan cart setelah added
		echo $this->list_cart(); //tampilkan cart setelah added
	}

	public function show_cart(){ //Fungsi untuk menampilkan Cart
		$output = '';
		$no = 0;
		foreach ($this->cart->contents() as $items) {
			$no++;
			$output .='
				<tr>
					<td>'.$items['name'].'</td>
					<td>'.number_format($items['price']).'</td>
					<td>'.$items['qty'].'</td>
					<td>'.number_format($items['subtotal']).'</td>
					<td><button type="button" id="'.$items['rowid'].'" class="hapus_cart btn btn-danger btn-xs">Hapus</button></td>
				</tr>
			';
		}
		$output .= '
			<tr>
				<th colspan="3">Total</th>
				<th colspan="2">'.'Rp '.number_format($this->cart->total()).'</th>
			</tr>
			<tr>
				<td colspan="5"><center><button type="submit" id="bayar" class="reset-cart btn bg-maroon">Bayar</button></center></td>
			</tr>
		';
		return $output;
	}

	public function list_cart(){ //Fungsi untuk menampilkan Cart
		$output = '';
		$no = 0;
		foreach ($this->cart->contents() as $items) {
			$no++;
			$output .='
				<li>
                    <a>            			
                      <h4>
                        <b>'.$items['name'].'</b>
                        <small><button type="button" id="'.$items['rowid'].'" class="hapus_cart btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              			<i class="fa fa-times"></i></button></small>
                      </h4>
                      <p>'.$items['qty'].'</p>
                    </a>
                </li>
			';
		}
		return $output;
	}

	public function detail_cart(){ //Fungsi untuk menampilkan Cart
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "user") {
			$this->load->model('Aksesoris_model');
			$data['acc'] = $this->Aksesoris_model->getAksesoris();
			$data['content'] = 'user/detail_cart';
			$this->load->view('templates/user', $data);
		} else {
			$this->load->model('Aksesoris_model');
			$data['acc'] = $this->Aksesoris_model->getAksesoris();
			$data['content'] = 'detail_cart';
			$this->load->view('templates/guest', $data);
		}
	}

	public function count_cart(){
		$no = 0;
		foreach ($this->cart->contents() as $items){
		$no++;
		}
		echo $no;

		return $no;
	}

	public function load_cart(){ //load data cart
		echo $this->show_cart();
	}
	public function load_list_cart(){ //load data cart
		echo $this->list_cart();
	}

	public function hapus_cart(){ //fungsi untuk menghapus item cart
		$data = array(
			'rowid' => $this->input->post('row_id'), 
			'qty' => 0, 
		);
		$this->cart->update($data);
		echo $this->show_cart();
		echo $this->list_cart();
	}

	public function reset_cart(){
		foreach ($this->cart->contents() as $items) {
			$data = array(
				'rowid' => $this->input->post('row_id'), 
				'qty' => 0, 
			);

			$this->cart->update($data);
		}
			$this->cart->update($data);	
	}

	// --------------- Cart ---------------------------------------------------------------------------

	// --------------- Purchase / Pembelian -----------------------------------------------------------

	public function purchase()
	{	
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			$this->load->model('Aksesoris_model');
			$cek = $this->Aksesoris_model->getPembelian();
			$data['acc'] = $cek->result();
			$data['content'] = 'admin/purchase';
			$this->load->view('templates/admin', $data);
		} else {
			$this->index();
		}
	}

	public function purchase_now()
	{	
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "admin") {
			$this->load->model('Aksesoris_model');
			$where = array('tanggal' => date('Y-m-d'));
			$cek = $this->Aksesoris_model->getPembelianNow($where);
			$data['acc'] = $cek->result();
			$data['content'] = 'admin/purchase';
			$this->load->view('templates/admin', $data);
		} else {
			$this->index();
		}
	}

	public function count_purchase(){
		$this->load->model('Aksesoris_model');
		$where = array('tanggal' => date('Y-m-d'));
		$cek = $this->Aksesoris_model->getPembelianNow($where);
		$jumlah = $cek->num_rows();

		echo $jumlah;
		return $jumlah;
	}
	public function list_purchase(){
		$no = 0;
		$this->load->model('Aksesoris_model');
		$where = array('tanggal' => date('Y-m-d'));
		$cek = $this->Aksesoris_model->getPembelianNow($where);
		$data = $cek->result();
		$row = json_encode($data);
		$results = json_decode($row, true);
		
		$output = '';

		foreach ($results as $items) {
			$no++;
		echo $output ='
				<li>
                    <a>            			
                      <h4>
                        <b>'. $items["email_pembeli"] .'</b>
                        <small>'. $items["jumlah"].'</small>
                      </h4>
                      <p>'.$items["nama_brg"].'</p>
                    </a>
                </li>

			';
		}
		return $output;
	}

	public function pembelian()
	{
		if (!$this->session->userdata('email') == "" && !$this->session->userdata('password') == "" && $this->session->userdata('hak_akses') == "user") {

			foreach ($this->cart->contents() as $items) {
			$id_brg = $items['id'];
			$nama_brg = $items['name'];
			$total_harga = $items['subtotal'];
			$jumlah = $items['qty'];
			$email = $this->session->userdata('email');
			$this->load->model('Aksesoris_model');	
				$acc = array(
					'id_brg' => $id_brg,
					'email_pembeli' => $email,
					'nama_brg' => $nama_brg,
					'jumlah' => $jumlah,
					'total_harga' => $total_harga,
					'tanggal' => date('Y-m-d')
				);
				$status = $this->Aksesoris_model->insertPembelian($acc);
				$data['acc'] = $this->Aksesoris_model->getAksesoris();
			}
				
				$this->user();
				$this->cart->destroy();

		} else {
			$this->cart->destroy();
			$this->login();
		}
	}

	// --------------- Purchase / Pembelian ---------------------------------------------------

	// --------------- Validation -------------------------------------------------------------	

	public function _rules()
	{
		$this->form_validation->set_rules('nama_brg', 'nama barang', 'trim|required')	;
		$this->form_validation->set_rules('harga_brg', 'harga barang', 'trim|required')	;
		$this->form_validation->set_rules('jumlah_brg', 'jumlah barang', 'trim|required')	;
		$this->form_validation->set_rules('kategori', 'kategori', 'trim|required')	;
		// $this->form_validation->set_rules('gambar', 'gambar', 'trim|required')	;
		$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>')	;
	}

	public function _rules_register()
	{
		$this->form_validation->set_rules('user', 'user', 'trim|required')	;
		$this->form_validation->set_rules('email', 'email', 'trim|required')	;
		$this->form_validation->set_rules('password1', 'password', 'trim|required')	;
		$this->form_validation->set_rules('password2', 'Retype password', 'trim|required')	;
		$this->form_validation->set_rules('check', 'Checked', 'trim|required')	;
		$this->form_validation->set_error_delimiters('<small><p class="text-danger">', '</p></small>')	;
	}

	// --------------- Validation -----------------------------------------------------------

	// --------------- Other ----------------------------------------------------------------

	private function _uploadImage()
	{
	    $config['upload_path']          = 'D:\xampp\htdocs\Tugas2-Rekweb-Kamis16-163040116-Fadhli\assets\images';
	    $config['allowed_types']        = 'gif|jpg|png';
	    $config['file_name']            = $this->input->post('id_brg');
	    $config['overwrite']			= true;
	    $config['max_size']             = 1024; // 1MB
	    // $config['max_width']            = 1024;
	    // $config['max_height']           = 768;
	    $this->load->library('upload', $config);
	    if ($this->upload->do_upload('gambar_brg')) {
	        return $this->upload->data("file_name");
	    }
	    
	    return "default.jpg";
	}

}
