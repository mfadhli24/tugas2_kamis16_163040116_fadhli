-- phpMyAdmin SQL Dump
-- version 4.8.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 08, 2018 at 06:34 PM
-- Server version: 10.1.34-MariaDB
-- PHP Version: 5.6.37

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `aksesoris`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_brg` varchar(11) NOT NULL,
  `nama_brg` char(30) NOT NULL,
  `harga_brg` int(11) NOT NULL,
  `stok_brg` int(11) NOT NULL,
  `kategori` char(30) NOT NULL,
  `gambar_brg` char(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_brg`, `nama_brg`, `harga_brg`, `stok_brg`, `kategori`, `gambar_brg`) VALUES
('Acc01', 'Casio Analog Digital', 500000, 5, 'JamTangan', 'Acc01.jpg'),
('Acc06', 'Blue Hat', 70000, 20, 'Topi', 'Acc06.jpg'),
('Acc07', 'Black White Hat', 85000, 10, 'Topi', 'Acc07.jpg'),
('Acc08', 'Back Hat', 50000, 55, 'Topi', 'Acc08.jpg'),
('Acc09', 'Cowboy Hat', 50000, 5, 'Topi', 'Acc09.jpg'),
('Acc10', 'Gelang Kulit Twin', 20000, 50, 'Gelang', 'Acc10.jpg'),
('Acc11', 'Gelang Jangkar', 15000, 30, 'Gelang', 'Acc11.jpg'),
('Acc12', 'Gelang Naga', 40000, 20, 'Gelang', 'Acc12.jpg'),
('Acc13', 'Gelang Kuling', 50000, 10, 'Gelang', 'Acc13.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `data_pengguna`
--

CREATE TABLE `data_pengguna` (
  `id` int(11) NOT NULL,
  `user` char(35) NOT NULL,
  `email` char(35) NOT NULL,
  `alamat` char(50) DEFAULT NULL,
  `kode_pos` char(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `data_pengguna`
--

INSERT INTO `data_pengguna` (`id`, `user`, `email`, `alamat`, `kode_pos`) VALUES
(8, 'M Fadhli Hakim', 'fadhli@gmail.com', '', ''),
(9, 'Rizal Amirudin', 'rizal@gmail.com', '', ''),
(10, 'Thomas Syahrizal S', 'thomas@gmail.com', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `pembelian`
--

CREATE TABLE `pembelian` (
  `id_pembelian` int(11) NOT NULL,
  `id_brg` varchar(11) NOT NULL,
  `email_pembeli` char(50) NOT NULL,
  `nama_brg` char(50) NOT NULL,
  `jumlah` int(11) NOT NULL,
  `total_harga` int(11) NOT NULL,
  `tanggal` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pembelian`
--

INSERT INTO `pembelian` (`id_pembelian`, `id_brg`, `email_pembeli`, `nama_brg`, `jumlah`, `total_harga`, `tanggal`) VALUES
(126, 'Acc02', 'rizal@gmail.com', 'Waist Bag', 9, 540000, '2018-12-07'),
(127, 'Acc03', 'rizal@gmail.com', 'Kacamata Pantai', 7, 210000, '2018-12-07'),
(128, 'Acc02', 'thomas@gmail.com', 'Waist Bag', 6, 360000, '2018-12-07'),
(129, 'Acc01', 'thomas@gmail.com', 'Sling Bag', 5, 250000, '2018-12-07'),
(130, 'Acc04', 'thomas@gmail.com', 'Back Pack', 5, 350000, '2018-12-07'),
(131, 'Acc03', 'thomas@gmail.com', 'Kacamata Pantai', 5, 150000, '2018-12-07'),
(132, 'Acc07', 'rizal@gmail.com', 'Black White Hat', 1, 85000, '2018-12-08'),
(133, 'Acc06', 'rizal@gmail.com', 'Blue Hat', 1, 70000, '2018-12-08'),
(134, 'Acc08', 'rizal@gmail.com', 'Back Hat', 1, 50000, '2018-12-08');

-- --------------------------------------------------------

--
-- Table structure for table `pengguna`
--

CREATE TABLE `pengguna` (
  `user` char(20) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` char(50) NOT NULL,
  `hak_akses` char(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `pengguna`
--

INSERT INTO `pengguna` (`user`, `password`, `email`, `hak_akses`) VALUES
('M Fadhli Hakim', '40982132d33fc2f7e9ce479eaf838117', 'fadhli@gmail.com', 'admin'),
('Rizal Amirudin', '150fb021c56c33f82eef99253eb36ee1', 'rizal@gmail.com', 'user'),
('Thomas Syahrizal S', 'ef6e65efc188e7dffd7335b646a85a21', 'thomas@gmail.com', 'user');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_brg`);

--
-- Indexes for table `data_pengguna`
--
ALTER TABLE `data_pengguna`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD PRIMARY KEY (`id_pembelian`);

--
-- Indexes for table `pengguna`
--
ALTER TABLE `pengguna`
  ADD PRIMARY KEY (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `data_pengguna`
--
ALTER TABLE `data_pengguna`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `pembelian`
--
ALTER TABLE `pembelian`
  MODIFY `id_pembelian` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=135;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `pembelian`
--
ALTER TABLE `pembelian`
  ADD CONSTRAINT `pembelian_ibfk_1` FOREIGN KEY (`id_brg`) REFERENCES `barang` (`id_brg`),
  ADD CONSTRAINT `pembelian_ibfk_2` FOREIGN KEY (`email_pembeli`) REFERENCES `pengguna` (`email`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
